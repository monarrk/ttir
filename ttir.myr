use std
use bio

/* represent an item on the stack */
type item = union
	`None
	`Int int
	`Atom std.strbuf#
;;

/* a dynamically sized stack */
type stack = struct
	/* stack index */
	idx : std.size 
	/* the current length of buf */
	len : std.size
	/* the stack itself */
	buf : item[:]
;;

/* create a stack */
const mkstack = { -> stack#
	var s

	s = std.zalloc()
	s.buf = std.slalloc(1)
	s.len = 1
	s.idx = 0

	-> s
}

/* free a stack */
const freestack = { stack : stack#
	/* free the Atom strbufs */
	for i : stack.buf
		match i
		| `Atom a: std.sbfree(a)
		| e: continue
		;;
	;;
	std.slfree(stack.buf)
	std.free(stack)
}

/* return the item on top of the stack */
const peek = { s : stack#
	if (s.idx - 1) >= s.len
		std.fatal("Index {} out of bounds! (len is {})\n", (s.idx - 1), s.len)
	;;

	-> s.buf[--s.idx]
}

/* push to the stack, growing if necessary */
const push = { s : stack#, i : item
	if s.idx >= s.len
		s.len *= 2
		std.slgrow(&s.buf, s.len)
	;;
	s.buf[s.idx++] = i
}

/* pop an item off the stack */
const pop = { s : stack# -> item
	var i

	/* safety rail */
	if (s.idx - 1) >= s.len
		std.fatal("Index {} out of bounds! (len is {})\n", (s.idx - 1), s.len)
	;;

	i = s.buf[--s.idx]
	s.buf[s.idx] = `None

	-> i
}

/* pop() but only accept integers */
/* TODO(@monarrk) : make errors more helpful */
const popint = { s : stack# -> int
	var i
	match pop(s)
	| `Int n: i = n
	| e: std.fatal("\"{}\" not allowed in this context!\n", e)
	;;
	-> i
}

/* popint() but return the top two off the stack. helpful for math functions */
const poptwoint = { s : stack# -> (int, int)
	var i1, i2
	i1 = popint(s)
	i2 = popint(s)
	-> (i1, i2)
}

/* 
 * print stack
 * I did this because std.put() prints hex for pointers, which makes strbufs awkward
 */
const display = { stack : stack#
	std.put("LENGTH: {}, ITEMS: [", stack.len)
	for i : stack.buf
		match i
		| `Atom a: std.put("ATOM :: \"{}\", ", std.sbpeek(a))
		| `Int v: std.put("INT :: {}, ", v)
		| `None: std.put("NONE, ")
		;;
	;;
	/* clear the trailing ", " */
	std.put("\b\b]\n")
}

/* push a string onto the stack, parsing out its type */
/* TODO(@monarrk) : DO NOT ACCEPT SPACES AND SPECIAL CHARACTERS IN IDENTIFIERS */
const pushstr = { s : stack#, str : std.strbuf#
	match std.intparse(std.sbpeek(str))
	| `std.Some i: push(s, `Int i)
	| `std.None: push(s, `Atom str)
	;;
}

/* pop the last two items off the stack and push their sum */
const add = { s : stack#
	var a, b
	(a, b) = poptwoint(s)
	push(s, `Int (a + b))
}

/* pop the last two items off the stack and return their difference */
const sub = { s : stack#
	var a, b
	(a, b) = poptwoint(s)
	push(s, `Int (a - b))
}

/* pop the last two items off the stack and return their product */
const mul = { s : stack#
	var a, b
	(a, b) = poptwoint(s)
	push(s, `Int (a * b))
}

/* pop the last two items off the stack and return their quotient */
const div = { s : stack#
	var a, b
	(a, b) = poptwoint(s)
	push(s, `Int (a / b))
}

/* 
 * TTIR COMMANDS
 *
 * A	push A
 * <	pop
 * +	add the stack
 * -	sub the stack
 * *	mul the stack
 * /	div the stack
 */
const run = { file
	var stack : stack#
	stack = mkstack()

	while true
		/* match the character type */
		match bio.readln(file)
		/* errors */
		| `std.Err `bio.Eof: break
		| `std.Err e: std.fatal("err: {}\n", e)
		
		/* empty string; ignore */
		| `std.Ok "": continue

		/* math */
		| `std.Ok "+": add(stack)
		| `std.Ok "-": sub(stack)
		| `std.Ok "/": div(stack)
		| `std.Ok "*": mul(stack)
		| `std.Ok "<": pop(stack)

		/* identifier */
		| `std.Ok c:
			var sb
			sb = std.mksb()
			std.sbputs(sb, c)
			pushstr(stack, sb)
		;;
	;;

	display(stack)
	freestack(stack)
}

const main = { args : byte[:][:]
	if args.len < 2
		std.put("Usage: {} [FILE]\n", args[0])
		std.exit(-1)
	;;

	/* pass the file to the "run" function to execute the file */
	match bio.open(args[1], bio.Rd)
	| `std.Ok bio: run(bio)
	| `std.Err msg: std.fatal("Error opening file: {}", msg)
	;;
}
