all: ttir

ttir: ttir.myr
	mbld -b ttir ttir.myr

clean:
	rm -f *.core ttir ttir.o ttir.use out.s a.out

run: all
	./ttir test.tt
